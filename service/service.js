var BASE_URL = "https://636150a5af66cc87dc28e460.mockapi.io/";

export var getProductList = () => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  });
};

export var getSingleProduct = (id) => {
  return axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "GET",
  });
};

export var setSingleProduct = (item) => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "POST",
    data: item,
  });
};

export var delProduct = (id) => {
  return axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "DELETE",
  });
};

export var putProduct = (id, item) => {
  return axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "PUT",
    data: item,
  });
};
