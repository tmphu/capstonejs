import { Product } from "../model/model.js";

export function getFormData() {
  var name = document.querySelector("#TenSP").value;
  var type = document.querySelector("#LoaiSP").value;
  var price = document.querySelector("#GiaSP").value;
  var screen = document.querySelector("#ManHinhSP").value;
  var frontCamera = document.querySelector("#CamTruoc").value;
  var backCamera = document.querySelector("#CamSau").value;
  var img = document.querySelector("#HinhSP").value;
  var desc = document.querySelector("#MoTa").value;

  return {
    name,
    type,
    price,
    screen,
    frontCamera,
    backCamera,
    img,
    desc,
  };
}

export function createSingleProduct(item) {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    item;
  return new Product(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
}

export function createProductList(data) {
  let productList = [];
  data.forEach((item) => {
    productList.push(createSingleProduct(item));
  });
  return productList;
}

export function renderProductList(list) {
  let contentHTML = "";
  list.forEach((item, index) => {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    contentHTML += `
      <tr>
        <td>${id}</td>
        <td>${type}</td>
        <td>${name}</td>
        <td>${price}</td>
        <td>${screen}</td>
        <td>${frontCamera}</td>
        <td>${backCamera}</td>
        <td><img src="${img}" height="60" /></td>
        <td>${desc}</td>
        <td>
        <button onClick="deleteProduct(${id})"
            class="btn btn-danger">Xóa</button>
        <button onClick="getProductInfo(${id})"
            class="btn btn-primary"
            id="btnSua"
            data-toggle="modal"
            data-target="#myModal"
            >Sửa</button>
        </td>
      </tr>
      `;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

export function showDataForm(item) {
  let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
    item;
  document.querySelector("#Id").value = id;
  document.querySelector("#TenSP").value = name;
  document.querySelector("#LoaiSP").value = type;
  document.querySelector("#GiaSP").value = price;
  document.querySelector("#ManHinhSP").value = screen;
  document.querySelector("#CamTruoc").value = frontCamera;
  document.querySelector("#CamSau").value = backCamera;
  document.querySelector("#HinhSP").value = img;
  document.querySelector("#MoTa").value = desc;
}

export function resetForm() {
  document.getElementById("formProduct").reset();
}

export function showMessage(id, message) {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "unset";
}

export function clearMessage(id) {
  document.getElementById(id).innerHTML = "";
  document.getElementById(id).style.display = "";
}
