import {
  getProductList,
  getSingleProduct,
  setSingleProduct,
  delProduct,
  putProduct,
} from "../service/service.js";
import {
  getFormData,
  createSingleProduct,
  createProductList,
  renderProductList,
  resetForm,
  showDataForm,
} from "./controller-admin.js";
import { validateProduct } from "../controller/validate.js";

var currentProductList = [];

var fetchAllProducts = () => {
  getProductList()
    .then((res) => {
      currentProductList = createProductList(res.data);
      renderProductList(currentProductList);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchAllProducts();

document.querySelector("#btnThem").addEventListener("click", function () {
  resetForm();

  // enable button add product
  document.getElementById("btnThemSP").style.display = "inline-block";

  // hide button update product
  document.getElementById("btnCapNhat").style.display = "none";
});

function addNewProduct() {
  let data = getFormData();
  let isValid = validateProduct(data);
  if (isValid) {
    setSingleProduct(data)
      .then((res) => {
        fetchAllProducts();
        resetForm();
        closeModal();
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Có lỗi xảy ra",
          text: "Không thể tạo sản phẩm mới, vui lòng thử lại",
        });
      });
  }
}
window.addNewProduct = addNewProduct;

function closeModal() {
  $(document).ready(function () {
    $("#myModal").modal("hide");
  });
}

function deleteProduct(id) {
  delProduct(id)
    .then((res) => {
      Swal.fire("Xóa sản phẩm thành công!");
      fetchAllProducts();
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        title: "Có lỗi xảy ra",
        text: "Không thể xóa sản phẩm, vui lòng thử lại",
      });
    });
}
window.deleteProduct = deleteProduct;

function getProductInfo(id) {
  document.getElementById("btnThemSP").style.display = "none";
  document.getElementById("btnCapNhat").style.display = "inline-block";
  getSingleProduct(id)
    .then((res) => {
      showDataForm(res.data);
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        title: "Có lỗi xảy ra",
        text: "Không thể sửa sản phẩm, vui lòng thử lại",
      });
    });
}
window.getProductInfo = getProductInfo;

function updateProduct() {
  let data = getFormData();
  let isValid = validateProduct(data);
  if (isValid) {
    putProduct(document.getElementById("Id").value, data)
      .then((res) => {
        Swal.fire("Cập nhật sản phẩm thành công!");
        fetchAllProducts();
        resetForm();
        closeModal();
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Có lỗi xảy ra",
          text: "Không thể cập nhật sản phẩm, vui lòng thử lại",
        });
      });
  }
}
window.updateProduct = updateProduct;

function searchProduct() {
  let inputKeyword = document.getElementById("inputTenSP").value;

  if (inputKeyword.length == 0) {
    fetchAllProducts();
    return;
  }

  let searchedArr = [];
  currentProductList.forEach(function (item) {
    if (item.name.toUpperCase().match(inputKeyword.toUpperCase())) {
      searchedArr.push(item);
    }
  });
  renderProductList(searchedArr);
}
window.searchProduct = searchProduct;

//listen event of Enter keyword
document
  .getElementById("inputTenSP")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnTimSP").click();
    }
  });
